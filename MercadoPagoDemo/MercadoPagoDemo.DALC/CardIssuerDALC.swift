//
//  CardIssuerDALC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/4/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class CardIssuerDALC {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func listCardIssuers(paymentMethodID: String, notificationID: String){
    
        let url = appDelegate.mainURL + "card_issuers?public_key=" + appDelegate.publicKey + "&payment_method_id=" + paymentMethodID
        
        Alamofire.request(url).responseArray { (response: DataResponse<[CardIssuerBE]>) in
            
            let listCardIssuers = response.result.value
            
            NotificationCenter.default.post(name: Notification.Name(rawValue:notificationID), object: listCardIssuers)
        }
    
    }
    
}
