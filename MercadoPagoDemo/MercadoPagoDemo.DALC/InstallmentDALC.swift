//
//  InstallmentDALC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/6/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class InstallmentDALC {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func listInstallments (amount: String, paymentMethodID: String, issuerID: String, notificationID: String){
        
        let url = appDelegate.mainURL + "installments?public_key=" + appDelegate.publicKey + "&amount=" + amount + "&payment_method_id=" + paymentMethodID + "&issuer.id=" + issuerID
        
        Alamofire.request(url).responseArray { (response: DataResponse<[InstallmentBE]>) in
            
            let listInstallments = response.result.value
            
            NotificationCenter.default.post(name: Notification.Name(rawValue:notificationID), object: listInstallments)
        }
        
    }
    
}
