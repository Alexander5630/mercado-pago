//
//  PaymentMethodDALC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class PaymentMethodDALC {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func listPaymentMethods (notificationID: String){
        
        let url = appDelegate.mainURL + "?public_key=" + appDelegate.publicKey
        
        Alamofire.request(url).responseArray { (response: DataResponse<[PaymentMethodBE]>) in
            
            let listPaymentMethods = response.result.value

             NotificationCenter.default.post(name: Notification.Name(rawValue:notificationID), object: listPaymentMethods)
        }
        
    }
    
}
