//
//  StepFourViewController.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/6/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import UIKit

class StepFourViewController: UIViewController {

    @IBOutlet weak var installmentsTableView: UITableView!
    
    @IBOutlet weak var issuerImageView: UIImageView!
    
    @IBOutlet weak var issuerLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    var listInstallments : [InstallmentBE] = []
    
    var listPayerCost : [PayerCostBE] = []
    
    var amount : String? = ""
    
    var paymentMethodBE = PaymentMethodBE()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !listInstallments.isEmpty{
            
            let installmentBE = listInstallments[0]
            
            let url = URL(string: (installmentBE.issuer?.secure_thumbnail)!)
            
            issuerImageView.kf.setImage(with: url)
            
            issuerLabel.text = installmentBE.issuer?.name
            
            listPayerCost = installmentBE.payer_costs!
            
            amountLabel.text = "Monto: $ " + amount!
            
        }else{
            listInstallments = []
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}


extension StepFourViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let payerCostBE = listPayerCost[indexPath.row]
        
        let rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "StepOneViewController") as! StepOneViewController
        
        rootViewController.isLoad = true
        rootViewController.paymentMethodBE = paymentMethodBE
        rootViewController.installmentBE = listInstallments[0]
        rootViewController.payerCostBE = payerCostBE
        rootViewController.amount = amount!
        
        let mainPageNav = UINavigationController(rootViewController: rootViewController)
        
        self.present(mainPageNav, animated: true, completion: nil)
        
    }
    
    
}

extension StepFourViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPayerCost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StepFourTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepFourTableViewCell") as! StepFourTableViewCell
        
        let payerCostBE = listPayerCost[indexPath.row]
        
        cell.nameLabel.text = payerCostBE.recommended_message

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell:StepFourTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepFourTableViewCell") as! StepFourTableViewCell
        
        return cell.frame.height
    }
    
}

