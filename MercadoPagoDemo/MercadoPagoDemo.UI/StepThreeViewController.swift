//
//  StepThreeViewController.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/4/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import UIKit
import Kingfisher

class StepThreeViewController: UIViewController {

    @IBOutlet weak var cardIssuersTableView: UITableView!
    
    var listCardIssuers : [CardIssuerBE] = []
    
    var amount : String? = ""
    
    var paymentMethodBE = PaymentMethodBE()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func listInstallments(_ notification:Notification) {
        
        let installmentsArray = (notification as NSNotification).object as! [InstallmentBE]
        
        if !installmentsArray.isEmpty {
            
            performSegue(withIdentifier: "installmentsSegue", sender: installmentsArray)
            
        }else{
            let alert = UIAlertController(title: "Mercado Pago", message: "En estos momentos no podemos atenderlo, por favor intente nuevamente en unos minutos", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("listInstallments"), object: nil);
        
    }
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "installmentsSegue" {
            
            let vc: StepFourViewController = segue.destination as! StepFourViewController
            vc.listInstallments = (sender as! [InstallmentBE])
            vc.amount = amount
            vc.paymentMethodBE = paymentMethodBE
            
        }
        
        
    }

}

extension StepThreeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cardIssuer = listCardIssuers[indexPath.row]
        
        NotificationCenter.default.addObserver(self, selector: #selector(listInstallments(_:)), name:NSNotification.Name(rawValue: "listInstallments"), object: nil)
        
        InstallmentBC.listInstallments(amount: amount!, paymentMethodID: (paymentMethodBE?.id)!, issuerID: cardIssuer.id!, notificationID: "listInstallments")
        
        
    }
    
    
}

extension StepThreeViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCardIssuers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StepThreeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepThreeTableViewCell") as! StepThreeTableViewCell
        
        let cardIssuer = listCardIssuers[indexPath.row]
        
        cell.nameLabel.text = cardIssuer.name
        
        let url = URL(string: cardIssuer.secure_thumbnail!)
        
        cell.thumbnailImageView.kf.setImage(with: url)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell:StepThreeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepThreeTableViewCell") as! StepThreeTableViewCell
        
        return cell.frame.height
    }
    
}
