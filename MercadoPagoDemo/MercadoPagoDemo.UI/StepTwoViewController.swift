//
//  StepTwoViewController.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import UIKit
import Kingfisher



class StepTwoViewController: UIViewController {

    @IBOutlet weak var paymentMethodsTableView: UITableView!
    
    var listPaymentMethods : [PaymentMethodBE] = []
    
    var amount : String? = ""
    
    var paymentMethodBE = PaymentMethodBE()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func listCardIssuers(_ notification:Notification) {
        
        let cardIssuersArray = (notification as NSNotification).object as! [CardIssuerBE]
        
        if !cardIssuersArray.isEmpty {
            
            performSegue(withIdentifier: "cardIssuersSegue", sender: cardIssuersArray)
            
        }else{
            let alert = UIAlertController(title: "Mercado Pago", message: "En estos momentos no podemos atenderlo, por favor intente nuevamente en unos minutos", preferredStyle: UIAlertControllerStyle.alert)

            alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("listCardIssuers"), object: nil);
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "cardIssuersSegue" {
            
            let vc: StepThreeViewController = segue.destination as! StepThreeViewController
            vc.listCardIssuers = (sender as! [CardIssuerBE])
            vc.amount = amount
            vc.paymentMethodBE = paymentMethodBE
            
        }
        
        
    }
 

}

extension StepTwoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let paymentMethodBE = listPaymentMethods[indexPath.row]
        
        NotificationCenter.default.addObserver(self, selector: #selector(listCardIssuers(_:)), name:NSNotification.Name(rawValue: "listCardIssuers"), object: nil)
        
        self.paymentMethodBE = paymentMethodBE
        
        CardIssuerBC.listCardIssuers(paymentMethodID: paymentMethodBE.id!, notificationID: "listCardIssuers")
     
    }
    
    
}

extension StepTwoViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPaymentMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StepTwoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepTwoTableViewCell") as! StepTwoTableViewCell
        
        let paymentMethod = listPaymentMethods[indexPath.row]
        
        cell.nameLabel.text = paymentMethod.name
        
        let url = URL(string: paymentMethod.secure_thumbnail!)
        
        cell.thumbnailImageView.kf.setImage(with: url)

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell:StepTwoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StepTwoTableViewCell") as! StepTwoTableViewCell
        
        return cell.frame.height
    }
    
}
