//
//  StepTwoTableViewCell.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import UIKit

class StepTwoTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
