//
//  StepOneViewController.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import UIKit

class StepOneViewController: UIViewController {
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var paymentMethodsButton: UIButton!
    
    var isLoad : Bool? = false
    var paymentMethodBE = PaymentMethodBE()
    var installmentBE = InstallmentBE()
    var payerCostBE = PayerCostBE()
    var amount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isLoad! {
            
            let message = " Monto:" + amount + " \n Medio de Pago: " + (paymentMethodBE?.name)! + "\n Tarjeta: " + (installmentBE?.issuer?.name)! + " \n " + (payerCostBE?.recommended_message)!
            
            let alert = UIAlertController(title: "PAGO REALIZADO CON ÉXITO", message: message, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    @objc func listPaymentMethods(_ notification:Notification) {
        
        let paymentMethodsArray = (notification as NSNotification).object as! [PaymentMethodBE]
        
        if !paymentMethodsArray.isEmpty {
            
            performSegue(withIdentifier: "paymentMethodsSegue", sender: paymentMethodsArray)
            
        }else{
           let alert = UIAlertController(title: "Mercado Pago", message: "En estos momentos no podemos atenderlo, por favor intente nuevamente en unos minutos", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("listPaymentMethods"), object: nil);
        
    }
 
    @IBAction func paymentMethodsButtonTapped(_ sender: UIButton) {
        
        if amountTextField.text == "" {
            
            let alert = UIAlertController(title: "Mercado Pago", message: "Por favor ingrese un monto válido", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            NotificationCenter.default.addObserver(self, selector: #selector(listPaymentMethods(_:)), name:NSNotification.Name(rawValue: "listPaymentMethods"), object: nil)
            
            PaymentMethodBC.listPaymentMethods(notificationID: "listPaymentMethods")
            
        }
        
        
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "paymentMethodsSegue" {
            
            let vc: StepTwoViewController = segue.destination as! StepTwoViewController
            vc.listPaymentMethods = (sender as! [PaymentMethodBE]).filter{($0.payment_type_id?.contains("credit_card"))!}
            vc.amount = amountTextField.text
            
        }
        
    }
 

}
