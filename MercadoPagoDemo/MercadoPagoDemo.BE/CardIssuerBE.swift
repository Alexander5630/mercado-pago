//
//  CardIssuerBE.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/4/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import ObjectMapper

class CardIssuerBE: Mappable {
    
    var id : String?
    
    var name : String?
    
    var secure_thumbnail : String?
    
    var thumbnail : String?
    
    var processing_mode : String?
    
    var merchant_account_id : String?
    
    required init?(map: Map) {
        
    }
    
    required init?() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        secure_thumbnail <- map["secure_thumbnail"]
        thumbnail <- map["thumbnail"]
        processing_mode <- map["processing_mode"]
        merchant_account_id <- map["merchant_account_id"]
    }
}
