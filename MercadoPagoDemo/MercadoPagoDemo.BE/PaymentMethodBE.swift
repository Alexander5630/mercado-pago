//
//  PaymentMethodBE.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentMethodBE : Mappable {
    
    var id : String?
    
    var name : String?
    
    var payment_type_id : String?
    
    var status : String?
    
    var secure_thumbnail : String?
    
    var thumbnail : String?
    
    var deferred_capture : String?
    
    var min_allowed_amount : String?
    
    var max_allowed_amount : String?
    
    var accreditation_time : String?
     
    required init?(map: Map) {
        
    }
    
    required init?() {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        payment_type_id <- map["payment_type_id"]
        status <- map["status"]
        secure_thumbnail <- map["secure_thumbnail"]
        thumbnail <- map["thumbnail"]
        deferred_capture <- map["deferred_capture"]
    }
    
}
