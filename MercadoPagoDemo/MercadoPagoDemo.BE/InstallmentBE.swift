//
//  InstallmentBE.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/5/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import ObjectMapper

class InstallmentBE: Mappable {
    
    var payment_method_id : String?
    
    var payment_type_id : String?
    
    var issuer : CardIssuerBE?
    
    var processing_mode : String?
    
    var merchant_account_id : String?
    
    var payer_costs : [PayerCostBE]?
    
    required init?(map: Map) {
        
    }
    
    required init?() {
        
    }
    
    func mapping(map: Map) {
        payment_method_id <- map["payment_method_id"]
        payment_type_id <- map["payment_type_id"]
        issuer <- map["issuer"]
        processing_mode <- map["processing_mode"]
        merchant_account_id <- map["merchant_account_id"]
        payer_costs <- map["payer_costs"]
    }
    
}
