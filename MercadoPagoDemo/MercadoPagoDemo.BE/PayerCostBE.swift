//
//  PayerCostBE.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/6/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation
import ObjectMapper

class PayerCostBE : Mappable {
    
    var installments : Int?
    
    var installment_rate : Int?
    
    var discount_rate : Int?
    
    var labels : [String]?
    
    var installment_rate_collector : [String]?
    
    var min_allowed_amount : Int?
    
    var max_allowed_amount : Int?
    
    var recommended_message : String?
    
    var installment_amount : Int?
    
    var total_amount : Int?
    
    required init?(map: Map) {
        
    }
    
    required init?() {
        
    }
    
    func mapping(map: Map) {
        installments <- map["installments"]
        installment_rate <- map["installment_rate"]
        discount_rate <- map["discount_rate"]
        labels <- map["labels"]
        installment_rate_collector <- map["installment_rate_collector"]
        min_allowed_amount <- map["min_allowed_amount"]
        max_allowed_amount <- map["max_allowed_amount"]
        recommended_message <- map["recommended_message"]
        installment_amount <- map["installment_amount"]
        total_amount <- map["total_amount"]
    
    
    }
    
}
