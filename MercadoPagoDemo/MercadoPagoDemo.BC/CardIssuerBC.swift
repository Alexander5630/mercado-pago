//
//  CardIssuerBC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/5/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation

class CardIssuerBC {
    
    
    static func listCardIssuers(paymentMethodID: String, notificationID: String){
        
        let cardIssuerDALC = CardIssuerDALC()
        cardIssuerDALC.listCardIssuers(paymentMethodID: paymentMethodID, notificationID: notificationID)
        
    }
    
}
