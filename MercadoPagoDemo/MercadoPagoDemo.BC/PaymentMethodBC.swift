//
//  PaymentMethodBC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/1/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation

class PaymentMethodBC {
    
    
    static func listPaymentMethods (notificationID: String){
        
        let paymentMethodDALC =  PaymentMethodDALC()
        paymentMethodDALC.listPaymentMethods(notificationID: notificationID)
        
    }
    
    
}
