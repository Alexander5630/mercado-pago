//
//  InstallmentBC.swift
//  MercadoPagoDemo
//
//  Created by rogeradmin on 9/6/18.
//  Copyright © 2018 Roger Arroyo. All rights reserved.
//

import Foundation

class InstallmentBC {
    
    static func listInstallments (amount: String, paymentMethodID: String, issuerID: String, notificationID: String){
    
        let installmentDALC = InstallmentDALC()
        installmentDALC.listInstallments(amount: amount, paymentMethodID: paymentMethodID, issuerID: issuerID, notificationID: notificationID)
    
    }
}
